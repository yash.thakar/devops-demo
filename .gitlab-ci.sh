#!/bin/bash

# Install dependencies only for Docker.
[[ ! -e /.dockerinit ]] && [[ ! -e  /.dockerenv ]] && exit 0

# Enable modules
docker-php-ext-enable zip
docker-php-ext-enable pdo_mysql
docker-php-ext-enable mcrypt

git config --global user.email echo "$USER_EMAIL_ID"
git config --global user.name echo "$USER_NAME"

# install ssh-agent
# which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )

# run ssh-agent
# eval $(ssh-agent -s)

# add ssh key stored in SSH_PRIVATE_KEY variable to the agent store
# ssh-add <(echo "$SSH_PRIVATE_KEY")

##
## Add the SSH key stored in SSH_PRIVATE_KEY variable to the agent store
## We're using tr to fix line endings which makes ed25519 keys work
## without extra base64 encoding.
## https://gitlab.com/gitlab-examples/ssh-private-key/issues/1#note_48526556
##
# echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null

# disable host key checking (NOTE: makes you susceptible to man-in-the-middle attacks)
# WARNING: use only in docker container, if you use it with shell you will overwrite your user's ssh config
# mkdir -p ~/.ssh
# echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config

# try to connect to GitLab.com
# ssh git@gitlab.com:yash.thakar/devops-demo.git

# Install project dependencies.
composer install --no-suggest --prefer-dist

# Copy over testing configuration.
# cp .env.example .env

# Generate an application key. Re-cache.
# php artisan key:generate
# php artisan config:cache
# php artisan config:clear